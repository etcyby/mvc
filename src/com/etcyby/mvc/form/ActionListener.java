package com.etcyby.mvc.form;

import java.io.IOException;
import java.util.Map;

import javax.servlet.ServletContext;
import javax.servlet.ServletRequestEvent;
import javax.servlet.ServletRequestListener;

import org.jdom2.JDOMException;

import com.etcyby.mvc.model.XmlBean;
import com.etcyby.mvc.utils.XmlParse;

public class ActionListener implements ServletRequestListener {

    public void requestDestroyed(ServletRequestEvent arg0) {
        // TODO Auto-generated method stub

    }

    public void requestInitialized(ServletRequestEvent arg0) {
        ServletContext context = arg0.getServletContext();
        String tomcatPath = context.getRealPath("\\");
        String xmlPath = context.getInitParameter("struts-config");
        try {
            Map<String, XmlBean> map = XmlParse.struts_xml(tomcatPath+xmlPath);
            context.setAttribute("struts", map);
        } catch (JDOMException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        } catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        } catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

    }

}
