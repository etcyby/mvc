package com.etcyby.mvc.action;

import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.etcyby.mvc.form.ActionForm;

public interface Action {

	String execute(HttpServletRequest request, HttpServletResponse response, ActionForm form, Map<String, String> map);
}
