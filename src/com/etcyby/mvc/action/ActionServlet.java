package com.etcyby.mvc.action;

import java.io.IOException;
import java.util.Map;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.etcyby.mvc.form.ActionForm;
import com.etcyby.mvc.form.FullForm;
import com.etcyby.mvc.model.XmlBean;

public class ActionServlet extends HttpServlet {

	/**
	 * 
	 */
	private static final long serialVersionUID = 700296676723369227L;

	/**
	 * Constructor of the object.
	 */
	public ActionServlet() {
		super();
	}

	/**
	 * Destruction of the servlet. <br>
	 */
	public void destroy() {
		super.destroy(); // Just puts "destroy" string in log
		// Put your code here
	}

	public void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		//System.out.println(this.getPath(request.getServletPath()));
		//获取请求头
		String path = this.getPath(request.getServletPath());
		
		Map<String,XmlBean> map = (Map<String,XmlBean>) this.getServletContext().getAttribute("struts");
		XmlBean xml = map.get(path);
		String formclass = xml.getFormClass();
		ActionForm form = FullForm.full(formclass, request);
		String actionClass = xml.getActionClass();
		Action action = null;
		String url = "";
		try
		{
			Class clazz = Class.forName(actionClass);
			action 		= (Action) clazz.newInstance();
			url = action.execute(request, response, form, xml.getActionForward());
		}catch (Exception e) {
			e.printStackTrace();
			System.out.println("严重：控制器异常！");
		}
//		String url = "";
//		Action action = new PandAction();
//		url = action.execute(request);
//		//获得了转发器对象
		RequestDispatcher dis = request.getRequestDispatcher(url);
		dis.forward(request, response);		
	}

	public void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		this.doGet(request, response);
	}

	/**
	 * Initialization of the servlet. <br>
	 * 
	 * @throws ServletException
	 *             if an error occurs
	 */
	public void init() throws ServletException {
		// Put your code here
	}
	
	private String getPath(String servletpath)
	{
		return servletpath.split("\\.")[0];
	}
}
