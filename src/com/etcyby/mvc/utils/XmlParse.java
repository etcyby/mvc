package com.etcyby.mvc.utils;

import java.io.File;
import java.io.IOException;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.jdom2.Document;
import org.jdom2.Element;
import org.jdom2.input.SAXBuilder;

import com.etcyby.mvc.model.XmlBean;

public class XmlParse {
	
	public XmlParse()
	{
		
	}
	
	public static Map<String, XmlBean> struts_xml(String xmlpath) throws Exception
	{
		SAXBuilder builder 	= new SAXBuilder();
		Document document 	= builder.build(new File(xmlpath));
		Element root 		= document.getRootElement();
		Map<String, XmlBean> rmap = new HashMap<String, XmlBean>();
		Element actionroot = root.getChild("action-mapping");
		List<Element> actio = actionroot.getChildren();
		for(Element e:actio)
		{
			XmlBean action = new XmlBean();
			String name = e.getAttributeValue("name");
			action.setBeanName(name);
			Element actionform 	= root.getChild("form-beans");
			List<Element> form 	= actionform.getChildren();
			for(Element ex:form)
			{
				if(name.equals(ex.getAttributeValue("name")))
				{
					String formClass = ex.getAttributeValue("class");
					action.setFormClass(formClass);
					break;
				}
			}
			String path = e.getAttributeValue("path");
			action.setPath(path);
			String type = e.getAttributeValue("type");
			action.setActionClass(type);
			List<Element> forward = e.getChildren();
			Map<String, String> map = new HashMap<String, String>();
			for(Element x:forward)
			{
				String fname = x.getAttributeValue("name");
				String fvalue = x.getAttributeValue("value");
				map.put(fname, fvalue);
			}
			action.setActionForward(map);
			rmap.put(path, action);
		}
		return rmap;
	}
	
	public static void struts_xml_test() throws Exception
	{
		SAXBuilder builder 	= new SAXBuilder();
		Document document 	= builder.build(new File("bin/struts-config.xml"));
		Element root 		= document.getRootElement();
		Element actionform 	= root.getChild("formbeans");
		List<Element> form 	= actionform.getChildren();
		for(Element e:form)
		{
			String name = e.getAttributeValue("name");
			String clas = e.getAttributeValue("class");
			System.out.println("formbeans:name="+name+" || class="+clas);
		}
		Element actionroot = root.getChild("action-mapping");
		List<Element> actio = actionroot.getChildren();
		for(Element e:actio)
		{
			String name = e.getAttributeValue("name");
			String path = e.getAttributeValue("path");
			String type = e.getAttributeValue("type");
			List<Element> forward = e.getChildren();
			for(Element x:forward)
			{
				String fname = x.getAttributeValue("name");
				String fvalue = x.getAttributeValue("value");
				System.out.println("forward:name="+fname+" || value="+fvalue);
			}
			System.out.println("action-mapping:name="+name+" || path="+path+" || type="+type);
		}
	}
	
	static public void main(String arg[]) throws Exception
	{
		//Struts_xml.struts_xml_test();
		Map<String, XmlBean> m = XmlParse.struts_xml("bin/struts-config.xml");
		Set<String> keyset = m.keySet();
		for(Iterator<String> it = keyset.iterator(); it.hasNext();)
		{
			String key = it.next();
			System.out.println(key);
			System.out.println(m.get(key));
		}
	}
	

}
