package com.etcyby.example;

import com.etcyby.mvc.form.ActionForm;

public class UserModel extends ActionForm {
	
	private String name = "";
	private String address = "";
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getAddress() {
		return address;
	}
	public void setAddress(String address) {
		this.address = address;
	}
	
	public String toString()
	{
		
		return "name="+this.name+"||address=" +this.address;
	}
	
}
